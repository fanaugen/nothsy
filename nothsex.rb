# -*- coding: utf-8 -*-
require 'net/http'
require 'httparty'
require 'json'

raise "Environment variable NOTHS_TOKEN not set" unless ENV['NOTHS_TOKEN']

# thin wrapper for the notonthehighstreet API
class NothsApi
  include HTTParty
  base_uri 'https://api.notonthehighstreet.com/api'

  def initialize(api_token)
    @defaults = {
      headers: {
        'X-Notonthehighstreet-Token' => api_token,
        'Accept' => 'application/json'
      }
    }
  end

  def get(path, opts={})
    res = self.class.get(path, opts.merge!(@defaults))
    return res.code == 200 ? res.body : "HTTP status #{res.code}"
  end
end

class Recombinator
  @@pre = %w(a an and any about above across after around as at atop before behind below
    beneath beside between beyond by but except from given his her in including inside like many my
    near of off on onto out our outside over per plus pro since some than the this that these
    their those through to toward towards under underneath unlike until unto up upon with within
    without your)

  def initialize(api)
    @api = api
  end

  def generate
    begin
      titles = [1,2].map! do |i|
        get_random_product(get_random_partner)['title']
      end
    rescue Exception => e
      return "(404) not found"
    end
    left, right = titles.map do |title|
      smart_split(title)
    end.shuffle!

    cut = rand

    lcut, rcut = (cut * left.size).to_i, (cut * right.size).to_i
    left_part, right_part = left[0..lcut], right[rcut+1..-1]

    # edge cases
    if left_part.empty?
      left_part = left[0..0]
      right_part.shift
    elsif right_part.empty?
      right_part = right[-1..-1]
      left_part.pop
    end

    (left_part + right_part).join(' ')
  end

  private

  def smart_split(str)
    str.strip!
    str.squeeze!(' ')
    return [str.capitalize] unless str =~ /\s/

    # kill “smart” quotes
    @re_snglquote ||= /[‘’]/
    @re_dblquote  ||= /[“”«»„“]/
    str.gsub!(@re_snglquote, "'")
    str.gsub!(@re_dblquote,  '"')

    # keep quoted parts together
    @re_quoted ||= /((?<!\w)(['"])[^'"]+?\2)/i # strings in quotes, except that's and the like
    # keep prepositions with right neighbour
    @re_prep = %r/
      ( # remember the group
        (?: # don’t remember this group
          \b(?: # don’t remember this one
            #{@@pre.join '|'} # any preposition
          )\s # followed by whitespace
        )+ # and possibly another preposition with whitespace
      \S+) # followed by a word
    /ix

    arr = str.split(@re_quoted).map! do |substr|
      substr =~ @re_quoted ? substr : substr.split(@re_prep)
    end
    arr.flatten!

    arr.map! do |substr|
      if substr =~ @re_quoted || substr =~ @re_prep
        substr
      else
        substr.split(/\s+/)
      end
    end
    # clean up and return
    arr.flatten!
    arr.reject! {|part| part =~ /^\W*$/}
    arr.map! do |part|
      part.strip.split(/\s/).join(' ')
    end
    arr
  end

  def get_random_partner
    @partners_info ||= JSON.parse(@api.get('/partners?perPage=1'))['pagination']
    @partner_count ||= @partners_info['total_entries']
    @link_template ||= @partners_info['link_templates'].select do |t|
      t['rel'] =~ /partners$/
    end.first['href'].gsub('{page}','%d')

    partner_no = rand(1..@partner_count)
    partner = JSON.parse(@api.get(@link_template % partner_no))['partners'].first
  end

  def get_random_product(partner)
    products_link = partner['links'].select{|l| l['rel'] =~ /products$/}.first['href']
    products_info = JSON.parse(@api.get("#{products_link}?perPage=1"))['pagination']
    product_count = products_info['total_entries']
    random_product_link = products_info['link_templates'].select do |t|
      t['rel'] =~ /products$/
    end.first['href'].gsub!('{page}', rand(1..product_count).to_s)
    product = JSON.parse(@api.get(random_product_link))['products'].first
  end

end

if __FILE__ == $0

  def output(str)
    len = str.length
    horiz = "*" * (len + 6)
    puts "\n#{horiz}"
    vert = "*  #{' '*len}  *"
    puts vert
    puts "*  #{str}  *"
    puts vert
    puts "#{horiz}\n"
  end

  api = NothsApi.new(ENV['NOTHS_TOKEN'])
  nothsy = Recombinator.new(api)

  repeat = false
  begin
    output nothsy.generate
    puts "generate another? (y/n)"
    repeat = gets.chomp =~ /y/i
  end while repeat

end
